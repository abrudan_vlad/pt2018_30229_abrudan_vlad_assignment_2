package model;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import View.View;

public class Generate implements Runnable {
private int y;
private int minProcess, maxProcess, minArriveTime, maxArriveTime;
private List<Casa> casa=new ArrayList<>();
private int option;
private int peek=0;
private View view;
private int peakTime;

public Generate(int minProcess,int maxProcess,int minArriveTime,int maxArriveTime,int y, List<Casa> casa, int option, View view) {
	this.minProcess=minProcess;
	this.maxProcess=maxProcess;
	this.minArriveTime=minArriveTime;
	this.maxArriveTime=maxArriveTime;
	this.y=y;
	this.casa=casa;
	this.option=option;
	this.view=view;
}


public Client generateClient(int minProcess,int maxProcess,int minArriveTime, int maxArriveTime, int id) {
	Random arrive=new Random();
	Random processTime=new Random();
	int arriveTime =minArriveTime+arrive.nextInt(maxArriveTime-minArriveTime+1);
	int time=minProcess+processTime.nextInt(maxProcess-minProcess+1);
	Client client=new Client(arriveTime, time,id);
	return client;
}

public void run() {
	int nrClient=1;
	int finalTime=0;
	Client c=generateClient(minProcess, maxProcess, minArriveTime, maxArriveTime, nrClient);
	int t=c.getArrivingTime();
	for(int i=1;i<=y;i++) {
	    try {
	    	Thread.sleep(1000);
	    }catch(InterruptedException e){}
	    if(i==t) {
	    	int numarCasa=alegeCoada(option);
	    	view.putText(view.getEvolutionLog(),i+":Clientul "+nrClient+" are timpul de procesare "+c.getProcessTime()+" si s-a asezat la coada "+(numarCasa+1)+"\n");
	    	casa.get(numarCasa).addClient(c);
	    	finalTime=calculatePeakTime(t,finalTime);
	    	c.setNoCasa(numarCasa);
	    	String string="Client "+c.getId()+"\n";
	    	if(numarCasa==0)
	    		view.putText(view.getCoada1(), string);
	    	if(numarCasa==1)
	    		view.putText(view.getCoada2(), string);
	    	if(numarCasa==2)
	    		view.putText(view.getCoada3(), string);
	    	if(numarCasa==3)
	    		view.putText(view.getCoada4(), string);
	    	if(numarCasa==4)
	    		view.putText(view.getCoada5(), string);
	    	if(numarCasa==5)
	    		view.putText(view.getCoada6(), string);
	    	if(numarCasa==6)
	    		view.putText(view.getCoada7(), string);
	    	nrClient++;
	    	c=generateClient(minProcess, maxProcess, minArriveTime, maxArriveTime, nrClient);
	    	t+=c.getArrivingTime();  	
	    }
	}
	try {
		Thread.sleep(3000);
	} catch (Exception e) {}
	view.putText(view.getEvolutionLog(),"Ora de varf este la momentul "+finalTime+"\n");
	view.putText(view.getEvolutionLog(),"Timpul mediu de asteptare este:"+calculateAverageWaitingTime()+"\n");
}

public int alegeCoada(int a) {
	if(a==1) {
		int minimumWaitTime=1000;
		for(Casa c: casa) {
			if(c.getWaitingTime()<minimumWaitTime) {
				minimumWaitTime=c.getWaitingTime();
				peek=casa.indexOf(c);
			}
		}
	}
	if(a==2) {
		Random random=new Random();
		peek=random.nextInt(casa.size());
	}
	return peek;
}

public int calculatePeakTime(int currentTime,int finalTime) {
	int sum=0;
	for (Casa casa2 : casa) {
		sum+=casa2.getClienti().size();
	}
	if(sum>peakTime) {
		peakTime=sum;
		finalTime=currentTime;
	}
	return finalTime;
}

public double calculateAverageWaitingTime() {
	int sum=0;
	for (Casa casa2 : casa) {
		sum+=casa2.getAverageWaitingTime();
	}
	return sum/Integer.parseInt(view.getNrCozi().getText());
}
	
}
