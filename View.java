package View;
import java.awt.Dimension;
import java.awt.GridLayout; 

import javax.swing.*;


@SuppressWarnings("serial")
public class View extends JFrame {
	
private JLabel l1=new JLabel("Coada1");
private JLabel l2=new JLabel("Coada2");
private JLabel l3=new JLabel("Coada3");
private JLabel l4=new JLabel("Coada4");
private JLabel l5=new JLabel("Coada5");
private JLabel l6=new JLabel("Coada6");
private JLabel l7=new JLabel("Coada7");
private JLabel l8=new JLabel("Timp minim sosire");
private JLabel l9=new JLabel("Timp maxim sosire");
private JLabel l10=new JLabel("Timp minim procesare");
private JLabel l11=new JLabel("Timp maxim procesare");
private JLabel l12=new JLabel("Numar cozi");
private JLabel l13=new JLabel("Timp de simulare");
private JTextArea coada1=new JTextArea();// coada 1
private JTextArea coada2=new JTextArea();//coada 2 
private JTextArea coada3=new JTextArea();//coada 3
private JTextArea coada4=new JTextArea();//coada 4
private JTextArea coada5=new JTextArea();//coada 5
private JTextArea coada6=new JTextArea();//coada 6
private JTextArea coada7=new JTextArea();//coada 7
private JTextArea sosireMin=new JTextArea();//sosire min
private JTextArea sosireMax=new JTextArea();//sosire max
private JTextArea processMin=new JTextArea();//process min
private JTextArea processMax=new JTextArea();//process max
private JTextArea nrCozi=new JTextArea();//nr cozi
private JTextArea timpSimulare=new JTextArea();//timp simulare
private JButton btnStart=new JButton("START");
private JTextArea evolutionLog=new JTextArea();//log
private JComboBox<String> option=new JComboBox<>();
private JPanel p2=new JPanel();
private JPanel p17=new JPanel();

public View() {
	JPanel p1=new JPanel();
    arangeTop();
    arangeDown();
	p1.setLayout(new GridLayout(4, 1));
	p1.add(p2);
	p1.add(createScrollText(evolutionLog));
	p1.add(p17);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setSize(new Dimension(800,700));
	this.setLocation(400, 10);
	this.setContentPane(p1);
	this.setVisible(true);
}

public void setPane(JPanel p,JTextArea t,JLabel l) {
	p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
	t.setMaximumSize(new Dimension(200, 50));
	p.add(l);
	p.add(t);
}

public void setPane1(JPanel p,JScrollPane t,JLabel l) {
	p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
	t.setMaximumSize(new Dimension(150, 200));
	p.add(l);
	p.add(t);
}
public JScrollPane createScrollText(JTextArea a) {
	JScrollPane scrollPane=new JScrollPane(a, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	return scrollPane;
}
public void arangeTop() {
	String[] options= {"Minimal wait time","Random"};
	option.addItem(options[0]);
	option.addItem(options[1]);
	JPanel p3=new JPanel();
	JPanel p4=new JPanel();
	JPanel p5=new JPanel();
	JPanel p6= new JPanel();
	JPanel p7=new JPanel();
	JPanel p8=new JPanel();
	setPane(p3, sosireMin, l8);
	setPane(p4, sosireMax, l9);
	setPane(p5, processMin, l10);
	setPane(p6, processMax, l11);
	setPane(p7, nrCozi, l12);
	setPane(p8, timpSimulare, l13);
	p2.setLayout(new GridLayout(5, 2));
	p2.add(p3);
	p2.add(p4);
	p2.add(p5);
	p2.add(p6);
	p2.add(p7);
	p2.add(p8);
	p2.add(btnStart);
	p2.add(option);
}

public void arangeDown() {
	JPanel p10=new JPanel();
	JPanel p9=new JPanel();
	JPanel p11=new JPanel();
	JPanel p13=new JPanel();
	JPanel p14=new JPanel();
	JPanel p15=new JPanel();
	JPanel p16=new JPanel();
	setPane1(p9, createScrollText(coada1), l1);
	setPane1(p10, createScrollText(coada2), l2);
	setPane1(p11, createScrollText(coada3), l3);
	setPane1(p13, createScrollText(coada4), l4);
	setPane1(p14, createScrollText(coada5), l5);
	setPane1(p15, createScrollText(coada6), l6);
	setPane1(p16, createScrollText(coada7), l7);
	p17.setLayout(new GridLayout());
	p17.add(p9);
	p17.add(p10);
	p17.add(p11);
	p17.add(p13);
	p17.add(p14);
	p17.add(p15);
	p17.add(p16);
	p17.add(p16);	
}

public void putText(JTextArea textArea, String string) {
	textArea.setText(textArea.getText()+" "+string);
}

public JButton getStartButton() {
	return btnStart;
}
public JTextArea getSosireMax() {
	return sosireMax;
}
public JTextArea getSosireMin() {
	return sosireMin;
}
public JTextArea getCoada7() {
	return coada7;
}
public JTextArea getCoada6() {
	return coada6;
}
public JTextArea getCoada5() {
	return coada5;
}
public JTextArea getCoada4() {
	return coada4;
}
public JTextArea getCoada3() {
	return coada3;
}
public JTextArea getCoada2() {
	return coada2;
}
public JTextArea getEvolutionLog() {
	return evolutionLog;
}
public JTextArea getTimpSimulare() {
	return timpSimulare;
}
public JTextArea getNrCozi() {
	return nrCozi;
}
public JTextArea getProcessMax() {
	return processMax;
}
public JTextArea getProcessMin() {
	return processMin;
}
public JTextArea getCoada1() {
	return coada1;
}

public JComboBox<String> getOption() {
	return option;
}
}
