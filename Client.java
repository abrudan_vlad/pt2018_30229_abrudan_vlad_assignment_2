package model;
public class Client {
private int id;
private int timeToWait;
private int processTime;
private int arrivingTime;
private int noCasa;
public Client(int arrivingTime,int processTime,int id) {
	this.id=id;
	this.processTime=processTime;
	this.arrivingTime=arrivingTime;
}

public Client() {
	this.id=0;
	this.processTime=0;
}


public int getId(){
	return id;
}

public int getTimeToWait() {
	return timeToWait;
}

public int getProcessTime() {
	return processTime;
}

public void setTimeToWait(int time) {
	this.timeToWait=time;
}

public int getArrivingTime() {
	return arrivingTime;
	
}
public String toString() {
	return "Clientul "+id+" cu timpul de procesare "+processTime;
}

public int getNoCasa() {
	return noCasa;
}

public void setNoCasa(int noCasa) {
	this.noCasa = noCasa;
}
}
