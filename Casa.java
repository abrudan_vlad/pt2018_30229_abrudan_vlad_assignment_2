package model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import javax.swing.JTextArea;
import View.View;

public class Casa implements Runnable {
private BlockingQueue<Client> clienti=new ArrayBlockingQueue<Client>(15);
private int y;
private View view;
private int nrCasa;
private static List<JTextArea> windows;
private int averageWaitingTime;
private int amountOfClienti=0;

public Casa(int y, View view, int nrCasa) {
	this.y=y;
	this.view=view;
	this.nrCasa=nrCasa;
	windows=initializeWindows();
}

public void addClient(Client a) {
	try {
		clienti.put(a);
		averageWaitingTime+=a.getProcessTime();
		amountOfClienti++;
	} catch (Exception e) {}

}

public Client removeClient() {
		try {
			return clienti.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
}

public void run() {
	int time=0;
	while(true) {
		if(!clienti.isEmpty()) {
			time=clienti.peek().getArrivingTime();
			break;
		}
	}
	while(time<y) {
		if(!clienti.isEmpty()) {
			try {
				time+=clienti.peek().getProcessTime();
				if(time>y)
					break;
				Thread.sleep(clienti.peek().getProcessTime()*1000);
				Client client=removeClient();
				String string="";
				for (Client client1 : clienti) {
					string="Client "+client1.getId()+"\n";
				}
				windows.get(nrCasa).setText(string);
				view.putText(view.getEvolutionLog(),client+" a iesit de la coada\n" );
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
public int getWaitingTime() {
	int sum=0;
	for (Client client : clienti) {
		sum+=client.getProcessTime();
	}
	return sum;
}

	public String toString() {
		String string="";
		for (Client client : clienti) {
			string+="Clientul "+client.getId();
		}
		return string;
	}

	private List<JTextArea> initializeWindows() {
		List<JTextArea> windows=new ArrayList<>();
		windows.add(view.getCoada1());
		windows.add(view.getCoada2());
		windows.add(view.getCoada3());
		windows.add(view.getCoada4());
		windows.add(view.getCoada5());
		windows.add(view.getCoada6());
		windows.add(view.getCoada7());
		return windows;
	}
	
	public BlockingQueue<Client> getClienti() {
		return clienti;
	}
	
	public double getAverageWaitingTime() {
		return averageWaitingTime/amountOfClienti;
	}
	
}
