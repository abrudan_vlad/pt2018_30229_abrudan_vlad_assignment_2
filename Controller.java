package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import View.View;
import model.Casa;
import model.Generate;

public class Controller {
	private View view=new View();
	private Generate generate;
	private List<Casa> cases;

	public Controller() {
		view.getStartButton().addActionListener(new StartButtonListener());
	}
	
	public class StartButtonListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			cases=new ArrayList<>();
			for (int i=0;i<Integer.parseInt(view.getNrCozi().getText());i++) {
				cases.add(new Casa(Integer.parseInt(view.getTimpSimulare().getText()),view,i));
			}
			if(view.getOption().getSelectedItem().equals("Random"))
				generate=new Generate(Integer.parseInt(view.getProcessMin().getText()),Integer.parseInt(view.getProcessMax().getText()),Integer.parseInt(view.getSosireMin().getText()), Integer.parseInt(view.getSosireMax().getText()), Integer.parseInt(view.getTimpSimulare().getText()), cases,2,view);
			else
				generate=new Generate(Integer.parseInt(view.getProcessMin().getText()),Integer.parseInt(view.getProcessMax().getText()),Integer.parseInt(view.getSosireMin().getText()), Integer.parseInt(view.getSosireMax().getText()), Integer.parseInt(view.getTimpSimulare().getText()), cases,1,view);
			Thread generateThread= new Thread(generate);
			Thread caseThreads;
			for (Casa c : cases) {
				caseThreads=new Thread(c);
				caseThreads.start();
			}
			generateThread.start();
		}
		
	}
	
}
